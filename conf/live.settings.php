<?php

global $conf;

$is_drush = php_sapi_name() == 'cli';

if (!$is_drush) {
  // Site-specific redirects go here.

  /** Replace www.example.com with your registered domain name */
  $primary_domain = 'www.example.com';

  if ($_SERVER['HTTP_HOST'] != $primary_domain 
    || !isset($_SERVER['HTTP_USER_AGENT_HTTPS']) 
    || $_SERVER['HTTP_USER_AGENT_HTTPS'] != 'ON' ) {
    # Name transaction "redirect" in New Relic for improved reporting (optional)
    if (extension_loaded('newrelic')) { 
      newrelic_name_transaction("redirect"); 
    }
    header('HTTP/1.0 301 Moved Permanently');
    header('X-BOT-Debug-Info: settings.php redirect ' . __LINE__);
    header('Location: https://'. $primary_domain . $_SERVER['REQUEST_URI']);
    exit();
  }
  
  // Other Redirect Logic can go here...
  
}

@ini_set('session.cookie_lifetime',  0);

// SF Oauth (REST)
$conf['salesforce_consumer_key'] = "";
$conf['salesforce_consumer_secret'] = "";
$conf['salesforce_endpoint'] = "https://login.salesforce.com";

// monolog setting
$conf['monolog_channel_profiles']['watchdog'] = 'production';

if (isset($_SERVER['HTTP_HOST'])) {
  $domain = '.'. preg_replace('`^www.`', '', $_SERVER['HTTP_HOST']);
  if (count(explode('.', $domain)) > 2) {
    @ini_set('session.cookie_domain', $domain);
  }
}

$conf['rules_log_errors'] = 3;
$conf['rules_debug_log'] = 0;
$conf['rules_debug'] = 0;

$conf['error_level'] = 0;
$conf['cache'] = 1;
// I am enabling block caching, per Pantheon instructions...
$conf['block_cache'] = 1;
$conf['page_cache_maximum_age'] = 900;
$conf['preprocess_css'] = 1;
$conf['preprocess_js'] = 1;
$conf['page_compression'] = false;

$conf['habitat_disable_' . PANTHEON_ENVIRONMENT] = array(
  'environment_indicator',
  'habitat_ui',
  'update',
  'styleguide',
  'gelf',
);

$conf['maillog_send'] = 1;

$conf['habitat_enable_' . PANTHEON_ENVIRONMENT] = array(
  // Enable as appropriate:
  // 'googleanalytics',
  
);

// Habitat module should turn this off once the
// site is live, so may not need this anymore...
$conf['environment_indicator_overwritten_color'] = 'red';
$conf['environment_indicator_overwritten_text_color'] = 'white';

// MA Graylog settings.
// $conf['gelf_host'] = 'graylog.messageagency.com';
// $conf['gelf_port'] = 12201;
// Need to fill this in with the Jira Project Key ex. NCUSCR, SRDC, etc...
// $conf['gelf_jirakey'] = "";
