<?php

global $conf;

// SF Oauth (REST)
$conf['salesforce_consumer_key'] = "";
$conf['salesforce_consumer_secret'] = "";
$conf['salesforce_endpoint'] = "https://test.salesforce.com";

// Should test whether devel is able to be disabled prior to deployment
$conf['habitat_disable_' . PANTHEON_ENVIRONMENT] = array(
  'devel',
  'styleguide',
);
$conf['habitat_enable_' . PANTHEON_ENVIRONMENT] = array(
  'environment_indicator'
);

$conf['environment_indicator_overwritten_color'] = '#e99e01'; // yellow';
$conf['environment_indicator_overwritten_text_color'] = '#ffffff'; //white
