<?php

global $conf;

$conf['salesforce_consumer_key'] = "";
$conf['salesforce_consumer_secret'] = "";
$conf['salesforce_endpoint'] = "https://test.salesforce.com";

$conf['environment_indicator_overwritten_color'] = '#008000'; // green';
$conf['environment_indicator_overwritten_text_color'] = '#ffffff';
