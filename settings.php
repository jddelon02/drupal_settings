<?php

global $conf;

// Drush flag. Some things, like redirects, should be disabled for drush.
$is_drush = php_sapi_name() == 'cli';

// if (!empty($_SERVER['PANTHEON_SITE_NAME'])) {
//   $conf['site_mail'] = 'info+' . $_SERVER['PANTHEON_SITE_NAME'] . '@backofficethinking.com';
//   $conf['update_notify_emails'] = array('support+' . $_SERVER['PANTHEON_SITE_NAME'] . '@backofficethinking.com');
// }
// else {
//   $conf['site_mail'] = 'info@backofficethinking.com';
//   $conf['update_notify_emails'] = array('support@backofficethinking.com');
// }


// Composer Manager Default Directories.
$conf['composer_manager_file_dir'] = 'private://composer';
//$conf['composer_manager_vendor_dir'] = $_SERVER['HOME'] . '/code/vendor';

# Disable autobuild on Pantheon.
$conf['composer_manager_autobuild_file'] = 0;
$conf['composer_manager_autobuild_packages'] = 0;

// monolog setting default for all but live.
$conf['monolog_channel_profiles']['watchdog'] = 'development';

// Uncomment this if you're having trouble with imagecache generation.
// $conf['image_allow_insecure_derivatives'] = TRUE;

$conf['favicon_path'] = 'sites/all/themes/custom/images/favicon.ico';

// Global, all-environment ini
@ini_set('session.cookie_lifetime',  60 * 60 * 24 * 365);
@ini_set('magic_quotes_runtime',     0);
@ini_set('magic_quotes_sybase',      0);
@ini_set('session.cache_expire',     200000);
@ini_set('session.cache_limiter',    'none');
@ini_set('session.gc_maxlifetime',   200000);
@ini_set('session.save_handler',     'user');
@ini_set('session.use_only_cookies', 1);
@ini_set('session.use_trans_sid',    0);
@ini_set('url_rewriter.tags',        '');

$update_free_access = FALSE;
$drupal_hash_salt = '';
umask(0002);

$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|svg|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

$conf['habitat_variable'] = 'BOT_habitat';
$conf['habitat_habitats'] = array('dev', 'test', 'live', 'local');

$env_settings = '';
if (!defined('PANTHEON_ENVIRONMENT')) {
  $conf['BOT_habitat'] = 'local';
  $env_settings = __DIR__ . '/conf/local.settings.php';
}
else {
  // Redirect all web requests to HTTPS for pantheon.
  $https = $is_drush || (!empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') || (!empty($_SERVER['HTTP_X_SSL']) && $_SERVER['HTTP_X_SSL'] == 'ON');
  if (!$https) {
    header('HTTP/1.0 301 Moved Permanently');
    header('X-BOT-Debug: settings.php redirect ' . __LINE__);
    header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    exit();
  }
  $conf['https'] = TRUE;

  $env_settings = __DIR__ . '/conf/' . PANTHEON_ENVIRONMENT . '.settings.php';
  $conf['BOT_habitat'] = PANTHEON_ENVIRONMENT;

  // Use Environment Indicator for ALL PANTHEON environments if available.
  $conf['environment_indicator_overwrite'] = TRUE;
  $conf['environment_indicator_overwritten_position'] = 'bottom';
  $conf['environment_indicator_overwritten_fixed'] = FALSE;
  $conf['environment_indicator_overwritten_name'] = PANTHEON_ENVIRONMENT;

  // Use Redis for caching in ALL PANTHEON environments if available.
  // See https://pantheon.io/docs/redis/ for more info
  if (file_exists('sites/all/modules/contrib/redis/redis.autoload.inc')) {
    $conf['redis_client_interface'] = 'PhpRedis';
    $conf['cache_backends'][] = 'sites/all/modules/contrib/redis/redis.autoload.inc';
    $conf['cache_default_class'] = 'Redis_Cache';
    $conf['cache_prefix'] = array('default' => 'pantheon-redis');
    $conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
    $conf['lock_inc'] = 'sites/all/modules/contrib/redis/redis.lock.inc';
    $conf['page_cache_maximum_age'] = 900;
  }
}

// Default settings for all environments.
// Use includes to override per-environment.
$conf['habitat_disable_' . $conf['BOT_habitat']] = array(
  'update',
  'gelf'
);
$conf['habitat_enable_' . $conf['BOT_habitat']] = array(
  'devel',
  'environment_indicator',
  'maillog',
  'captcha',
  'honeypot',
  'recaptcha',
);

// Disable outgoing mail by default.
$conf['maillog_devel'] = 1;
$conf['maillog_log'] = 1;
$conf['maillog_send'] = 0;

// Enable rules debugging:
$conf['rules_log_errors'] = 2;
$conf['rules_debug_log'] = 1;
$conf['rules_debug'] = 1;
$conf['error_level'] = 2;

// Disable caching as much as possible:
$conf['cache'] = 0;
$conf['block_cache'] = 0;
$conf['page_cache_maximum_age'] = 0;
$conf['preprocess_css'] = 0;
$conf['preprocess_js'] = 0;
$conf['cache_lifetime'] = 0;
$conf['page_compression'] = false;

// Honeypot caching
$conf['honeypot_element_name'] = 'url';
$conf['honeypot_form_article_node_form'] = 0;
$conf['honeypot_form_feed_item_node_form'] = 0;
$conf['honeypot_form_feed_node_form'] =  0;
$conf['honeypot_form_page_node_form'] =  0;
$conf['honeypot_form_recipe_node_form'] = 0;
$conf['honeypot_form_user_pass'] = 1;
$conf['honeypot_form_user_register_form'] = 1;
$conf['honeypot_log'] = 0;
$conf['honeypot_protect_all_forms'] = 0;
$conf['honeypot_time_limit'] = '5';
$conf['honeypot_use_js_for_cached_pages'] = 0;

// Captcha/reCAPTCHA settings.
// Not sure if this should only be on live or not...
$conf['captcha_add_captcha_description'] = 1;
$conf['captcha_administration_mode'] = 0;
$conf['captcha_allow_on_admin_pages'] = 0;
$conf['captcha_default_challenge'] = 'recaptcha/reCAPTCHA';
$conf['captcha_default_challenge_on_nonlisted_forms'] = 0;
$conf['captcha_default_validation'] = 1;
$conf['captcha_description'] = 'This question is for testing whether or not you are a human visitor and to prevent automated spam submissions.';
$conf['captcha_enable_stats'] = 0;
$conf['captcha_log_wrong_responses'] = 0;
$conf['captcha_persistence'] = 1;
$conf['recaptcha_noscript'] = 0;
$conf['recaptcha_secret_key'] = '<SECRET KEY>';
$conf['recaptcha_site_key'] = '<SITE KEY>';

// Override sendgrid credentials per-client.
// $conf['smtp_on'] = TRUE;
// $conf['smtp_host'] = 'smtp.sendgrid.net';
// $conf['smtp_port'] = 25;
// $conf['smtp_username'] = 'apikey';
// $conf['smtp_password'] = ''; // Removed when made public
// $conf['smtp_from'] = $conf['site_mail'];
// $conf['smtp_fromname'] = $conf['site_mail'];

if (file_exists($env_settings)) {
  include_once $env_settings;
}

// Never set this value, never allow it to be set.
// cache_lifetime kills the Drupal.
$conf['cache_lifetime'] = 0;