# README #

Default settings.php, deployable for every Drupal install on every platform, including local dev and pantheon. 
### What is this repository for? ###

* settings.php
** Global settings which apply to all environments

* conf/
** environment-specific settings files

* conf/$env.settings.php
** environment-specific settings for environment $env, e.g. "dev", "live", etc.

### How do I get set up? ###

99.9% of deployments will include this package as a standalone set of files, disconnected from this repository.

For an existing site:
* backup existing settings.php files
* git clone (this repository)
* remove .git directory from the working copy
* merge changes as applicable

For additional documentation on settings.php, RTFM:
https://api.drupal.org/api/drupal/sites!default!default.settings.php/7.x